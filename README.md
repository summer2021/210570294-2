# Examples for go-storage

## Basic Operations

- [Create fs Storager](new_fs.go)
- [Create s3 Storager](new_s3.go) (Amazon S3)
- [Create cos Storager](new_cos.go) (Tencent Cloud Object Storage)
- [Create ipfs Storager](new_ipfs.go) (InterPlanetary File System)
- [Read a file](read.go)
- [Read a range of a file](read.go)
- [Read a file with callback](read.go)
- [Append to a new file](append.go)
- [Append to an existing file](append.go)
- [Multipart upload](multipart.go)
- [Resume a multipart upload](multipart.go)
- [Cancel a multipart upload](multipart.go)
