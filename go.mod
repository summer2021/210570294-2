module github.com/beyondstorage/go-storage-example

go 1.15

require (
	github.com/beyondstorage/go-service-cos/v2 v2.2.0
	github.com/beyondstorage/go-service-fs/v3 v3.3.0
	github.com/beyondstorage/go-service-ipfs v0.1.0
	github.com/beyondstorage/go-service-s3/v2 v2.3.0
	github.com/beyondstorage/go-storage/v4 v4.3.2
)
